@echo off
setlocal enabledelayedexpansion

if [%2]==[] (
REM Create Alias 1.1 dacook 2013-04-15
echo usage: create_alias alias_name alias_command
echo Creates an alias for the given command ^(spaces allowed^), stored in 
echo    user's .alias directory. Add to PATH for easy access.
goto :EOF
)

set PARMS=%2 %3 %4 %5 %6 %7 %8 %9
set ALIASPATH=%HOMEDRIVE%%HOMEPATH%\.alias

if not exist %ALIASPATH% mkdir %ALIASPATH%

REM Remove trailing whitespace
for /l %%a in (1,1,100) do if "!PARMS:~-1!"==" " set PARMS=!PARMS:~0,-1!

REM Create alias as a batch file
echo !PARMS! %%*> %ALIASPATH%\%1.bat

echo Done^^! Created alias "%1" for !PARMS!.